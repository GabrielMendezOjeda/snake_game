/**
 * Tests of Snake. 
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "SnakeWorld.c"
#include "Snake.c"
#include "Mouse.c"

void testGetLocation() {
    // Given

    // When

    // Then
	fail("Not yet implemented");
}

void testGetLocationAfterAdd() {
    // Given

    // When

    // Then
	fail("Not yet implemented");
}

void testAct() {
    // Given

    // When

    // Then
	fail("Not yet implemented");
}

void testActTwice() {
    // Given

    // When

    // Then
	fail("Not yet implemented");
}

void testActAfterClicked() {
    // Given

    // When

    // Then
	fail("Not yet implemented");
}

void testActAfterClickedTwice() {
    // Given

    // When

    // Then
	fail("Not yet implemented");
}

void testMouseIsNotInSnakeWorldBecauseItWasEaten() {
    // Given

    // When

    // Then
	fail("Not yet implemented");
}

void testMousesNumberIsZeroBecauseItWasEaten() {
    // Given

    // When

    // Then
	fail("Not yet implemented");
}
