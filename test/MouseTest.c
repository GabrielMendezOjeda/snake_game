/**
 * Tests of Snake. 
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "SnakeWorld.c"
#include "Mouse.c"

void testGetLocation() {
    // Given
	Actor Mouse = newActor ("Mouse");

    // When
	const char* location = getLocationMouse(Mouse);

    // Then
	assertEquals_String("out of the world", location);

}

void testGetLocationAfterAdd() {
    // Given

    // When

    // Then
	fail("Not yet implemented");
}
