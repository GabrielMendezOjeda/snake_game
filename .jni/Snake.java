import greenfoot.*;  

/**
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */
public class Snake extends Actor {
    
	public Snake() {
		start();
	}

    public void start(){
        start_();
    }
    private native void start_();
    
    public void act(){
        act_();
    }
    private native void act_();
    
    public String getLocation(){
        return getLocation_();
    }
    private native String getLocation_();
    
    static {
        System.load(new java.io.File(".jni", "Snake_jni.so").getAbsolutePath());
    }
}
