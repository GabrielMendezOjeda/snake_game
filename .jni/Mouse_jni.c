/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "Mouse.c"

#include "greenfoot.h"
extern JNIEnv  *javaEnv;

JNIEXPORT void JNICALL Java_Mouse_start_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    startMouse(object);
}

JNIEXPORT void JNICALL Java_Mouse_act_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    actMouse(object);
}

JNIEXPORT jstring JNICALL Java_Mouse_getLocation_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    return toJstring(getLocationMouse(object));
}
