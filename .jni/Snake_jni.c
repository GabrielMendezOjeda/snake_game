/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "Snake.c"

#include "greenfoot.h"
extern JNIEnv  *javaEnv;

JNIEXPORT void JNICALL Java_Snake_start_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    startSnake(object);
}

JNIEXPORT void JNICALL Java_Snake_act_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    actSnake(object);
}

JNIEXPORT jstring JNICALL Java_Snake_getLocation_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    return toJstring(getLocationSnake(object));
}
