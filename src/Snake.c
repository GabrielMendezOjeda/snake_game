/**
 * The Snake.c file defines a game actor that has a 50x40 size image built 
 * from the "snake.png" file. The actSnake() method declared in the Snake.c  
 * file defines the snake behaviour in each cycle of the scenario execution.
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "greenfoot.h"

/**
 * Initialize its image.
 */
void startSnake(Actor snake) {
	setImageFile(snake, "snake.png");
	setImageScale(snake, 50, 50);
}

void actSnake(Actor snake) {

}

const char* getLocationSnake(Actor snake) {
    
}
