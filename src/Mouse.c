/**
 * The Mouse.c file defines a game actor that has a 40x40 size image built 
 * from the "mouse.png" file. The actMouse() method declared in the Mouse.c  
 * file defines the mouse behaviour in each cycle of the scenario execution.
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "greenfoot.h"

/**
 * Initialize its image.
 */
void startMouse(Actor mouse) {
	setImageFile(mouse, "mouse.png");
	setImageScale(mouse, 40, 40);
}

void actMouse(Actor mouse) {

}

const char* getLocationMouse(Actor mouse) {
    
}
